#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-3.6.6-QtConsole-4.4.3

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use $MOD_DEST # not much needed
module add foss/2018b Python/3.6.6/maxiv/0.4 PyQt5/5.11.3-Python-3.6.6

module list

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment

# --- qtconsole ---
python -c "import qtconsole"
if [ $? -eq 0 ]
then
	echo "qtconsole python module already available, skipping installation qtconsole"
else
	# broken pip, error when using --no-cache-dir, repaired in later versions
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT qtconsole==4.4.3
	CWD=$(pwd)
	cd build
	git clone https://github.com/jupyter/qtconsole.git
	cd qtconsole; git checkout v4.4.3; git reset --hard
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD	
fi

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/QtConsole/
cp $SCDIR/modules/Python-3.6.6-QtConsole-4.4.3.lua $MOD_DEST/Python/3.6.6/QtConsole/4.4.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/QtConsole/4.4.3.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/QtConsole/4.4.3.lua

chmod o+r $MOD_DEST/Python/3.6.6/QtConsole/
chmod o+x $MOD_DEST/Python/3.6.6/QtConsole/

# End
echo 'DONE'
exit 0

