#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.12-silx-0.6.0

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 Python/2.7.12/PyQt5/5.7.1 CUDA/8.0.44-GCC-5.4.0-2.26

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- silx ----
python -c "import silx"
if [ $? -eq 0 ]
then
	echo "silx python module already available, skipping installation silx"
else
	rm -rfd silx-0.6.0
	#wget https://github.com/silx-kit/silx/archive/v0.6.0.tar.gz -O silx-0.6.0.tar.gz
	#tar -xf silx-0.6.0.tar.gz
	#CWD=$(pwd)
	#cd silx-0.6.0
        #python setup.py install --prefix=$SW_DEST/$PROJECT	
	#cd $CWD
	pip install -v --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT silx==0.6.0
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/silx/
cp modules/Python-2.7.12-silx-0.6.0.lua $MOD_DEST/Python/2.7.12/silx/0.6.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/silx/0.6.0.lua

# End
echo 'DONE'
exit 0

