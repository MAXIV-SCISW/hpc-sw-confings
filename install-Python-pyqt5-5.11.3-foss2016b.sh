#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.12-PyQt-5.11.3

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages
mkdir -p $SW_DEST/$PROJECT

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 Qt5/5.11.2 SIP/4.19.11-Python-2.7.12 opencl_cpu/16.1.1 CUDA/9.0.176-detached Python/2.7.12/maxiv/0.3

export CPATH=/sw/easybuild/software/MPI/GCC/5.4.0-2.26/OpenMPI/1.10.3/Python/2.7.12/include/python2.7:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- pyqt ----
python -c "import PyQt5"
if [ $? -eq 0 ]
then
	echo "PyQt5 python module already available, skipping installation PyQt5"
else
	rm -rfd PyQt5_gpl-5.11.3
	wget https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.11.3/PyQt5_gpl-5.11.3.tar.gz -O PyQt5_gpl-5.11.3.tar.gz
	tar -xf PyQt5_gpl-5.11.3.tar.gz
	CWD=$(pwd)
	cd PyQt5_gpl-5.11.3
	python configure.py --sysroot=$SW_DEST/$PROJECT --confirm-license --designer-plugindir=$SW_DEST/$PROJECT/QtPlugins/plugin/designer --qml-plugindir=$SW_DEST/$PROJECT/QtPlugins/PyQt5
	make
	make install
	cd $CWD	
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/PyQt5/
cp modules/Python-2.7.12-PyQt5-5.11.3.lua $MOD_DEST/Python/2.7.12/PyQt5/5.11.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/PyQt5/5.11.3.lua

# End
echo 'DONE'
exit 0

