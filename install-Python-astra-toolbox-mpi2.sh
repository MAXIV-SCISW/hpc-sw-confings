#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.3.0-2.27-OpenMPI-2.0.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.13-astra-toolbox-1.8 

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/python:$PYTHONPATH
#export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH
export LD_RUN_PATH=$SW_DEST/$PROJECT/lib:$LD_RUN_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.3.0-2.27 OpenMPI/2.0.2 Boost/1.63.0 Python/2.7.13/maxiv/0.1 matlab/R2017a

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- astra ----
python -c "import astra"
if [ $? -eq 0 ]
then
	echo "astra python module already available, skipping installation astra"
else
	rm -rfd astra-toolbox-1.8
	wget https://github.com/astra-toolbox/astra-toolbox/archive/v1.8.tar.gz -O astra-toolbox-1.8.tgz
	tar -xf astra-toolbox-1.8.tgz
	CWD=$(pwd)
	cd astra-toolbox-1.8/build/linux
	./autogen.sh   # when building a git version
	./configure --with-python \
              --with-matlab=/sw/pkg/matlab/R2017a \
              --with-install-type=prefix \
              --prefix=$SW_DEST/$PROJECT \
              --exec-prefix=$SW_DEST/$PROJECT
#							--with-cuda=/usr/local/cuda \
	make
  	make install-libraries 
	make install-python
	make install-matlab
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.13/astra-toolbox/
cp modules/Python-2.7.13-astra-toolbox-1.8.lua $MOD_DEST/Python/2.7.13/astra-toolbox/1.8.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.3.0-2.27-OpenMPI-2.0.2/g' $MOD_DEST/Python/2.7.13/astra-toolbox/1.8.lua

# End
echo 'DONE'
exit 0

