#!/bin/bash

#set -o nounset

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

YYYY=2019; MM=03; DD=22

PROJECT=Python-2.7.14-NanoPeakCell-0.3.3-$YYYY$MM$DD

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use /mxn/groups/pub/sw/modules/maxiv
module add opencl_cpu/16.1.1
module add foss/2018a gcccuda/2018a cctbx/20181029-Python-2.7.14 Python/2.7.14/pyFAI/0.17.0 CMake/3.10.2

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- Installation ----
python -c "import NPC"
if [ $? -eq 0 ]
then
	echo "NPC python module already available, skipping installation NanoPeakCell"
else
	# --- Dependencies ----
	pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT lz4==2.1.6
	pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyqtgraph==0.10.0
	# --- NanoPeakCell ----
	CWD=$(pwd)
	mkdir -p build; cd build
	rm -rfd NanoPeakCell
	git clone https://github.com/coquellen/NanoPeakCell.git
	cd NanoPeakCell
	git checkout master
        git reset --hard a99403aa18f9923064bc3531028969c7fdde187f
	python setup.py install --prefix=$SW_DEST/$PROJECT	
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/NanoPeakCell
cp modules/Python-2.7.14-npc-0.3.3-pyFAI-0.17.0-YYYYMMDD.lua $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua

sed -i -e 's/YYYY/'$YYYY'/g' $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/MM/'$MM'/g' $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/DD/'$DD'/g' $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/2.7.14/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua

chmod o+r $MOD_DEST/Python/2.7.14/NanoPeakCell
chmod o+x $MOD_DEST/Python/2.7.14/NanoPeakCell

# End
echo 'DONE'
exit 0

