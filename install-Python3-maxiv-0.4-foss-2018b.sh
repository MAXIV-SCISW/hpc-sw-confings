#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/7.3.0-2.30/OpenMPI/3.1.1

PROJECT=Python-3.6.6-maxiv-0.4

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages

# Start installing
module purge
module use $MOD_DEST # nod needed
module add opencl_cpu/16.1.1 # this must be on a separate line
module add matlab/R2018a # rather on a separate line
module add foss/2018b gcccuda/2018b  matplotlib/3.0.0-Python-3.6.6 bitshuffle/0.3.5-h5py-2.8.0-Python-3.6.6-HDF5-1.10.2 Boost/1.68.0-Python-3.6.6 ZeroMQ/4.2.5 libjpeg-turbo/2.0.0 LibTIFF/4.0.9 IPython/7.2.0-Python-3.6.6 scikit-image/0.14.1-Python-3.6.6 scikit-learn/0.20.0-Python-3.6.6 CMake/3.12.1

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# setuptools, testpath, ipython, jupyter, tornado, pyzmq, scikit-image, scikit-learn should come from IPython module now

# new path with easy_install (just in case)
export PATH=$SW_DEST/$PROJECT/bin:$PATH

pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw==0.11.1
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pybind11==2.2.4
# pybind11 deployed some CPP headers
export CPATH=$SW_DEST/$PROJECT/include/python3.6m:$CPATH
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pycuda==2018.1.1 # appdirs, pytools, py, attrs, more-itertools, atomicwrites, pluggy, pytest*, mako
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl==2018.2.2 # appdirs, pytools
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr==2.6.9
# pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio==0.8.0 # fabio installed in this way does not work
# troubles with testpath
CWD=$(pwd)
wget https://github.com/silx-kit/fabio/archive/v0.8.0.tar.gz -O fabio-0.8.0.tar.gz
tar -xf fabio-0.8.0.tar.gz
cd fabio-0.8.0
python setup.py install --prefix=$SW_DEST/$PROJECT
cd $CWD
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama==0.4.1
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy==2.0.11 # pluggy, pytest*
# astropy reinstalls pytest to an older version
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pytest==4.2.0 # for scikit-image 4.2.0 and astropy 3.6.4
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff==0.4.2
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py3==7.352.0
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl==3.1.0

# savu_magic
cp $SCDIR/savu_magick $SW_DEST/$PROJECT/bin/
cp $SCDIR/plot_slice-foss-2018a $SW_DEST/$PROJECT/bin/plot_slice

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/maxiv
cp $SCDIR/modules/Python-3.6.6-maxiv-0.4.lua $MOD_DEST/Python/3.6.6/maxiv/0.4.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/maxiv/0.4.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/maxiv/0.4.lua

chmod o+r $MOD_DEST/Python/3.6.6/maxiv/
chmod o+x $MOD_DEST/Python/3.6.6/maxiv/

# End
echo 'DONE'
exit 0

