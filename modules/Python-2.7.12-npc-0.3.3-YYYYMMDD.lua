-- *** Module definition comes here  ***

help([[
This package provides NanoPeakCell - serial crystallogr. preprocessing sw
	

	NanoPeakCell (NPC) is a python-based software intended to pre-process
serial crystallography data into ready-to-be-inedexed images with
CrystFEL, cctbx.xfel and nXDS. NPC is able to process data recorded
at SACLA and LCLS XFELS, as well as data recorded at any synchrotron
beamline. A graphical interface is deployed to visualize your raw
and pre-processed data.
]])

whatis("Version: 0.3.3-YYYY.MM.DD")
whatis("Keywords: python, serial crystallography, xfel, structural biology")
whatis("URL: https://github.com/coquellen/NanoPeakCell")
whatis("Description: NanoPeakCell - serial crystallography data preprocessing")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("cctbx/20180528-Python-2.7.12","PyQt/4.12-Python-2.7.12","Python/2.7.12/pyFAI/0.15.0")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-NanoPeakCell-0.3.3-YYYYMMDD/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-NanoPeakCell-0.3.3-YYYYMMDD/lib/python2.7/site-packages"))

setenv("NPC_ROOT", pathJoin(root,"Python-2.7.12-NanoPeakCell-0.3.3-YYYYMMDD"))

