-- *** Module definition comes here  ***

help([[
This package provides several common Python packeges and applications.
	
	Namely it provedes the following modules and applications:
		ipython jupyter pyfftw pyopencl Pillow scikit-image
		pywavelets pyzmq numexpr futures etc.
]])

whatis("Version: 0.1")
whatis("Keywords: Python, utilities")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: Some usefull python modules and applications")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.3.0-2.27","OpenMPI/2.0.2")
always_load("matplotlib/2.0.2-Python-2.7.13","h5py/2.7.0-Python-2.7.13-HDF5-1.10.1","FFTW/3.3.6","Boost/1.63.0","ZeroMQ/4.2.2","libjpeg-turbo/1.5.1","LibTIFF/4.0.9")

prepend_path("PATH", pathJoin(root,"/Python-2.7.13-maxiv-0.1/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"/Python-2.7.13-maxiv-0.1/lib/python2.7/site-packages"))
