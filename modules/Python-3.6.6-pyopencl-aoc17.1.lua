-- *** Module definition comes here  ***

help([[
This package provides pyopencl Python module for Altera/Intel OpenCL.
]])

whatis("Version: aoc-17.1")
whatis("Keywords: Python, OpenCL")
whatis("URL: https://wiki.maxiv.lu.se/index.php/KITS:FPGA_Accelerator_Node")
whatis("Description: PyOpenCL module for Altera/Intel OpenCL")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1","Python/3.6.6","fpga-sdk/intel/17.1")

prepend_path("PATH", pathJoin(root,"Python-3.6.6-pyopencl-aoc17.1/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-3.6.6-pyopencl-aoc17.1/lib/python3.6/site-packages"))
prepend_path("CPATH", pathJoin(root,"Python-3.6.6-pyopencl-aoc17.1/include/python3.6"))
prepend_path("CPATH", pathJoin(root,"Python-3.6.6-pyopencl-aoc17.1/include/python3.6m"))

setenv("PYOPENCL_ROOT", pathJoin(root,"Python-3.6.6-pyopencl-aoc17.1"))

