-- *** Module definition comes here  ***

help([[
This package provides Astra Toolbox - MATLAB and Python toolbox of CPU and high-
	performance GPU primitives for 2D and 3D tomography.
	
	2D parallel and fan beam geometries, and 3D parallel and cone beam are
	supported. All of them have highly flexible source/detector positioning.

	A large number of 2D and 3D algorithms are available, including FBP,
	SIRT, SART, CGLS.
]])

whatis("Version: 1.8")
whatis("Keywords: tomography, image reconstruction, python")
whatis("URL: http://www.astra-toolbox.com/")
whatis("Description: ASTRA Tomography Toolbox.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Boost/1.61.0","Python/2.7.12/maxiv/0.2","CUDA/8.0.44-GCC-5.4.0-2.26")

prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/lib"))
prepend_path("LD_RUN_PATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/lib"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/python"))
-- prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/lib/python2.7/site-packages"))

prepend_path("MATLABPATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/share/astra/matlab/mex"))
prepend_path("MATLABPATH", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8/share/astra/matlab/tools"))

setenv("ASTRATOOLBOX_ROOT", pathJoin(root,"Python-2.7.12-astra-toolbox-1.8"))

