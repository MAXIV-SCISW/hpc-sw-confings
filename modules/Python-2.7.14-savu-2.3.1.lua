-- *** Module definition comes here  ***

help([[
This package provides Savu - a Python package to assist with the processing and
	reconstruction of parallel-beam tomography data.	
]])

whatis("Version: 2.3.1")
whatis("Keywords: tomography, image reconstruction, python, DLS")
whatis("URL: https://savu.readthedocs.org/en/latest/")
whatis("Description: Savu - Tomography Reconstructon Pipeline.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("Python/2.7.14/maxiv/0.3","PyYAML/3.12-Python-2.7.14","Python/2.7.14/astra-toolbox/1.8.3","Python/2.7.14/tomopy/1.1.2","Python/2.7.14/xraylib/3.3.0")

prepend_path("PATH", pathJoin(root,"Python-2.7.14-savu-2.3.1/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.14-savu-2.3.1/lib/python2.7/site-packages"))

setenv("SAVU_ROOT", pathJoin(root,"Python-2.7.14-savu-2.3.1"))
