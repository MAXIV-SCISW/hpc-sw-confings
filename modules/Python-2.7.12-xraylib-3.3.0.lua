-- *** Module definition comes here  ***

help([[
This package provides xraylib python interface.
	
	Xraylib is a library of X-ray matter interaction cross sections
	for X-ray fluorescence applications.
]])

whatis("Version: 3.3.0")
whatis("Keywords: X-ray, fluorescence, cross section")
whatis("URL: https://github.com/tschoonj/xraylib/wiki")
whatis("Description: A library for X-ray matter interaction cross sections for X-ray fluorescence applications.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Python/2.7.12/maxiv/0.2")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/bin"))
prepend_path("CPATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/lib"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/lib/pkgconfig"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-xraylib-3.3.0/lib/python2.7/site-packages"))

setenv("XRAYLIB_ROOT", pathJoin(root,"Python-2.7.12-xraylib-3.3.0"))

