-- *** Module definition comes here  ***

help([[
This package provides Astra Toolbox - MATLAB and Python toolbox of CPU and high-
	performance GPU primitives for 2D and 3D tomography.
	
	2D parallel and fan beam geometries, and 3D parallel and cone beam are
	supported. All of them have highly flexible source/detector positioning.

	A large number of 2D and 3D algorithms are available, including FBP,
	SIRT, SART, CGLS.

	This module can be used with matlab/R2018b. Load the Matlab module
	possibly first before loading GCC, OpenMPI or foss modules.
]])

whatis("Version: 1.8.3")
whatis("Keywords: tomography, image reconstruction, python")
whatis("URL: http://www.astra-toolbox.com/")
whatis("Description: ASTRA Tomography Toolbox.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("Boost/1.66.0-Python-2.7.14","Python/2.7.14/maxiv/0.3")

prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/lib"))
prepend_path("LD_RUN_PATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/lib"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/python"))
-- prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/lib/python2.7/site-packages"))

prepend_path("MATLABPATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/share/astra/matlab/mex"))
prepend_path("MATLABPATH", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3/share/astra/matlab/tools"))

setenv("ASTRATOOLBOX_ROOT", pathJoin(root,"Python-2.7.14-astra-toolbox-1.8.3"))

