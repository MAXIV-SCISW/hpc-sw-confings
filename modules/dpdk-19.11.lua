-- *** Module definition comes here  ***

help([[
This module provides Data Plane Development Kit (DPDK).

	DPDK provides a set of data plane libraries and network interface controller
    polling-mode drivers for offloading TCP packet processing from the operating
    system kernel to processes running in user space.

]])

whatis("Version: 19.11")
whatis("Keywords: networking, TCP")
whatis("URL: https://www.dpdk.org/")
whatis("Description: Data Plane Development Kit.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")

prepend_path("PATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/lib/python3.6/site-packages"))

prepend_path("PATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/bin"))
prepend_path("CPATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/include"))
prepend_path("LIBRARY_PATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/lib"))
prepend_path("PATH", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/sbin"))

setenv("RTE_TARGET", "x86_64-native-linux-gcc")
setenv("RTE_SDK", pathJoin(root,"Python-3.6.6-dpdk-19.11/usr/local/share/dpdk"))

setenv("DPDK_ROOT", pathJoin(root,"Python-3.6.6-dpdk-19.11"))

