-- *** Module definition comes here  ***

help([[
This package provides several common Python packeges and applications.
	
	Namely it provedes the following modules and applications:
		ipython jupyter pyfftw pyopencl pycuda Pillow
		scikit-image pywavelets pyzmq numexpr futures
                pyopengl etc.
]])

whatis("Version: 0.3")
whatis("Keywords: Python, utilities")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: Some usefull python modules and applications")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("FFTW/3.3.4","matplotlib/1.5.3-Python-2.7.12","h5py/2.6.0-Python-2.7.12-HDF5-1.10.0-patch1","Boost/1.67.0-Python-2.7.12","ZeroMQ/4.2.5","libjpeg-turbo/1.5.1","LibTIFF/4.0.6")

prepend_path("PATH", pathJoin(root,"/Python-2.7.12-maxiv-0.3/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"/Python-2.7.12-maxiv-0.3/lib/python2.7/site-packages"))
