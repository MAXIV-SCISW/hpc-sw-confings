-- *** Module definition comes here  ***

help([[
This package provides TomoPy - an open-source Python package for tomographic
	data processing and image reconstruction.
	
	Features
	- Image reconstruction algorithms for tomography.
	- Various filters, ring removal algorithms, phase retrieval algorithms.
	- Forward projection operator for absorption and wave propagation.
]])

whatis("Version: 1.1.2")
whatis("Keywords: tomography, image reconstruction, python")
whatis("URL: http://tomopy.readthedocs.io/en/latest/")
whatis("Description: Tomographic Reconstruction in Python.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("Python/2.7.14/dxchange/0.1.5","imkl/2019.0.117")

prepend_path("PYTHONPATH", pathJoin(root,"/Python-2.7.14-tomopy-1.1.2/lib/python2.7/site-packages"))

setenv("TOMOPY_ROOT", pathJoin(root,"/Python-2.7.14-tomopy-1.1.2"))
