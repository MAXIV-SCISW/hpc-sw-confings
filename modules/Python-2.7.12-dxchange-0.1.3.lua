-- *** Module definition comes here  ***

help([[
This package provides dxchange - data exchange supporting tomopy.
]])

whatis("Version: 0.1.3")
whatis("Keywords: Python, tomography")
whatis("URL: https://github.com/data-exchange/dxchange")
whatis("Description: Python data exchange supporting tomopy.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Python/2.7.12/maxiv/0.2","netCDF/4.4.1")

prepend_path("PATH", pathJoin(root,"/Python-2.7.12-dxchange-0.1.3/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"/Python-2.7.12-dxchange-0.1.3/lib/python2.7/site-packages"))
