-- *** Module definition comes here  ***

help([[
This package provides several common Python packeges and applications.
	
	Namely it provedes the following modules and applications:
		ipython jupyter pyfftw pyopencl pycuda Pillow
                scikit-image dask scikit-learn pywavelets pyzmq
                numexpr futures pyopengl fabio libtiff etc.
]])

whatis("Version: 0.4")
whatis("Keywords: Python, utilities")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: Some usefull python modules and applications")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
always_load("FFTW/3.3.8","matplotlib/2.2.3-Python-2.7.15","bitshuffle/0.3.5-h5py-2.8.0-Python-2.7.15-HDF5-1.10.2","Boost/1.68.0-Python-2.7.15","ZeroMQ/4.2.5","libjpeg-turbo/2.0.0","LibTIFF/4.0.9","IPython/5.8.0-Python-2.7.15-foss2018b")

prepend_path("PATH", pathJoin(root,"Python-2.7.15-maxiv-0.4/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.15-maxiv-0.4/lib/python2.7/site-packages"))
prepend_path("CPATH", pathJoin(root,"Python-2.7.15-maxiv-0.4/include/python2.7"))

setenv("MAXIV_ROOT", pathJoin(root,"Python-2.7.15-maxiv-0.4"))

