-- *** Module definition comes here  ***

help([[
This package provides Jupyter QtConsole - enhanced interactive Python shell - Qt console.

	The Qt console is a very lightweight application that largely feels like a terminal,
	but provides a number of enhancements only possible in a GUI, such as inline figures,
	proper multi-line editing with syntax highlighting, graphical calltips, and much more.
	The Qt console can use any Jupyter kernel.

]])

whatis("Version: 4.4.3")
whatis("Keywords: jupyter, ipython, Qt, console")
whatis("URL: https://qtconsole.readthedocs.io")
whatis("Description: Jupyter QtConsole - enhanced interactive Python shell.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
always_load("Python/2.7.15/maxiv/0.4","PyQt5/5.11.3-Python-2.7.15-foss2018b")

prepend_path("PATH", pathJoin(root,"Python-2.7.15-QtConsole-4.4.3/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.15-QtConsole-4.4.3/lib/python2.7/site-packages"))

setenv("MPLBACKEND","Qt5Agg")

setenv("QTCONSOLE_ROOT", pathJoin(root,"Python-2.7.15-QtConsole-4.4.3"))
