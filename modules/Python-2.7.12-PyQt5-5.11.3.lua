-- *** Module definition comes here  ***

help([[
This package provides python bindings for Qt5.

	PyQt brings together the Qt C++ cross-platform application framework and
	the cross-platform interpreted language Python. 
]])

whatis("Version: 5.11.3")
whatis("Keywords: gui, widgets, python")
whatis("URL: https://www.riverbankcomputing.com/software/pyqt")
whatis("Description: A set of Python v2 and v3 bindings for the Qt application framework.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Qt5/5.11.2","SIP/4.19.11-Python-2.7.12","Python/2.7.12/maxiv/0.3")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/bin"))
prepend_path("CPATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/QtPlugins/plugin/decorator"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/QtPlugins/PyQt5"))
prepend_path("LIBRARY_PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/lib"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/lib/pkgconfig"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-PyQt-5.11.3/lib/python2.7/site-packages"))

setenv("PYQT5_ROOT", pathJoin(root,"Python-2.7.12-PyQt-5.11.3"))

