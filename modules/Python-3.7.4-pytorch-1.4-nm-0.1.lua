-- *** Module definition comes here  ***

help([[
This package provides PyTorch and Tensorflow for a NanoMAX beamtime.
	
	This package provides PyTorch, TensorFlow, OpenCV and related
python modules for a 20200007-proposal beamtime at NanoMAX.

    It is based mainly on fosscuda/2019b toolcahin and so it
requires the following modules will be loaded:

	GCC/8.3.0 CUDA/10.1.243 OpenMPI/3.1.4 

]])

whatis("Version: 0.1")
whatis("Keywords: python, ML, NanoMAX, CXI")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: PyTorch and Tensorflow for a NanoMAX beamtime.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/data/visitors/nanomax/20200007/2020042508/process/sw"

-- HACK for missing LUNARC modules in foffcuda/2019b
prepend_path("MODULEPATH", "/sw/easybuild/modules/all/MPI/GCC/8.3.0/OpenMPI/3.1.4")

-- MAX IV modules
prepend_path("MODULEPATH", "/mxn/groups/pub/sw/modules/maxiv")

-- main toolchain
always_load("fosscuda/2019b")

-- partial modules
always_load("IPython/7.9.0-Python-3.7.4","scikit-image/0.16.2-Python-3.7.4")
always_load("bitshuffle/0.3.5-h5py-2.10.0-Python-3.7.4-HDF5-1.10.5")
always_load("OpenCV/4.2.0-Python-3.7.4")
always_load("R-keras/2.2.5.0-Python-3.7.4-R-3.6.2","torchvision/0.5.0-Python-3.7.4")

-- sw not in global modules
prepend_path("PATH", pathJoin(root,"Python-3.7.4-pytorch-1.4-nm-0.1/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-3.7.4-pytorch-1.4-nm-0.1/lib/python3.7/site-packages"))

setenv("PYTORCH_NM_ROOT", pathJoin(root,"Python-3.7.4-pytorch-1.4-nm-0.1"))

