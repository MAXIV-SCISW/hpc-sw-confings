-- *** Module definition comes here  ***

help([[
This package provides pyFAI - Fast Azimuthal Integration in Python.
	
	pyFAI is an azimuthal integration library that tries to be fast
(as fast as C and even more using OpenCL and GPU). It is based
on histogramming of the 2theta/Q positions of each (center of)
pixel weighted by the intensity of each pixel, but parallel version
uses a SparseMatrix-DenseVector multiplication. Neighboring output
bins get also a contribution of pixels next to the border thanks
to pixel splitting. Finally pyFAI provides also tools to calibrate
the experimental setup using Debye-Scherrer rings of a reference
compound.
]])

whatis("Version: 0.15.0")
whatis("Keywords: python, diffraction, calibration, synchrotron")
whatis("URL: https://github.com/silx-kit/pyFAI")
whatis("Description: pyFAI - Fast Azimuthal Integration in Python.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Python/2.7.12/silx/0.8.0")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-pyfai-0.15.0/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-pyfai-0.15.0/lib/python2.7/site-packages"))

setenv("PYFAI_ROOT", pathJoin(root,"Python-2.7.12-pyfai-0.15.0"))
