-- *** Module definition comes here  ***

help([[
This package provides xrt - software for ray tracing and wave propagation in x-ray regime
	
        Package xrt (XRayTracer) is a python software library for ray tracing and
wave propagation in x-ray regime. It is primarily meant for modeling synchrotron
sources, beamlines and beamline elements. Includes a GUI for creating a beamline
and interactively viewing it in 3D.
]])

whatis("Version: 1.3.3")
whatis("Keywords: ray tracing, python, synchrotron, wave propagation")
whatis("URL: https://xrt.readthedocs.io/")
whatis("Description: xrt - python XRayTracer.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
always_load("Spyder/3.3.2-Python-3.6.6","Python/3.6.6/maxiv/0.4")

prepend_path("PATH", pathJoin(root,"Python-3.6.6-xrt-1.3.3/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-3.6.6-xrt-1.3.3/lib/python3.6/site-packages"))

setenv("MPLBACKEND", 'Qt5Agg')

setenv("XRT_ROOT", pathJoin(root,"Python-3.6.6-xrt-1.3.3"))
