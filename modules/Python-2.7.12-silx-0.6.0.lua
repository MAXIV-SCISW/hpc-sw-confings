-- *** Module definition comes here  ***

help([[
This package provides Silx - ScIentific Library for eXperimentalists.
	
	The toolkit aims at providing a collection of Python packages to support
the development of data assessment, reduction and analysis applications
at synchrotron radiation facilities. It aims at providing reading/writing
different file formats, data reduction routines and a set of Qt widgets to
browse and visualize data.

	The current version provides:
	- reading HDF5 file format (with support of SPEC file format and FabIO images)
	- histogramming
	- fitting
	- 1D and 2D visualization widgets using multiple backends (matplotlib or OpenGL)
	- an OpenGL-based widget to display 3D scalar field with isosurface and cutting plane
	- an image plot widget with a set of associated tools
	- a unified browser for HDF5, SPEC and image file formats supporting inspection
	  and visualization of n-dimensional datasets.
	- a unified viewer (silx view filename) for HDF5, SPEC and image file formats
	- a unified converter to HDF5 format (silx convert filename)
	- median filters on images (C and OpenCL implementations)
	- image alignment (sift - OpenCL implementation)
	- filtered backprojection for tomography
]])

whatis("Version: 0.6.0")
whatis("Keywords: python, hdf5, plotting, gui, synchrotron")
whatis("URL: https://github.com/silx-kit/silx")
whatis("Description: silx - ScIentific Library for eXperimentalists.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Python/2.7.12/PyQt5/5.7.1")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-silx-0.6.0/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-silx-0.6.0/lib/python2.7/site-packages"))

setenv("SILX_ROOT", pathJoin(root,"Python-2.7.12-silx-0.6.0"))
