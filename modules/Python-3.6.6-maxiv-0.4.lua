-- *** Module definition comes here  ***

help([[
This package provides several common Python packeges and applications.
	
	Namely it provedes the following modules and applications:
		ipython jupyter pyfftw pyopencl pycuda Pillow
                scikit-image dask scikit-learn pywavelets pyzmq
                numexpr pyopengl fabio libtiff etc.
]])

whatis("Version: 0.4")
whatis("Keywords: Python, utilities")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: Some usefull python modules and applications")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
always_load("FFTW/3.3.8","matplotlib/3.0.0-Python-3.6.6","bitshuffle/0.3.5-h5py-2.8.0-Python-3.6.6-HDF5-1.10.2","Boost/1.68.0-Python-3.6.6","ZeroMQ/4.2.5","libjpeg-turbo/2.0.0","LibTIFF/4.0.9","IPython/7.2.0-Python-3.6.6","scikit-image/0.14.1-Python-3.6.6","scikit-learn/0.20.0-Python-3.6.6")

prepend_path("PATH", pathJoin(root,"Python-3.6.6-maxiv-0.4/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-3.6.6-maxiv-0.4/lib/python3.6/site-packages"))
prepend_path("CPATH", pathJoin(root,"Python-3.6.6-maxiv-0.4/include/python3.6"))
prepend_path("CPATH", pathJoin(root,"Python-3.6.6-maxiv-0.4/include/python3.6m"))

setenv("MAXIV_ROOT", pathJoin(root,"Python-3.6.6-maxiv-0.4"))

