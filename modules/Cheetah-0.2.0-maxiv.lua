-- *** Module definition comes here  ***

help([[
This package provides Cheetah for Eiger - a program for processing serial diffraction
        data from Eiger detectors.

        It should allow for taking home only the data with meaningful content.

	Required: GCC/6.4.0-2.28 OpenMPI/2.1.2
]])

whatis("Version: 0.2.0-maxiv")
whatis("Keywords: serial crystallography, SX, hitfinding")
whatis("URL: http://www.desy.de/~barty/cheetah/Cheetah")
whatis("Description: a program for processing serial diffraction data from Eiger detectors.")

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("bitshuffle/0.3.5-h5py-2.8.0-Python-2.7.14-HDF5-1.10.2-ts-foss2018a","Boost/1.66.0-Python-2.7.14")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prepend_path("PATH", pathJoin(root, "Cheetah-eiger-0.2.0-maxiv/bin"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "Cheetah-eiger-0.2.0-maxiv/lib"))

setenv("CHEETAH_ROOT", pathJoin(root, "Cheetah-eiger-0.2.0-maxiv"))
