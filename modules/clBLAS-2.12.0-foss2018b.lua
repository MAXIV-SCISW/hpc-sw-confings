-- *** Module definition comes here  ***

help([[
This package provides clBLAS.

	Use 'export BLAS=$CLBLAS_ROOT/lib64/libclBLAS.so' if wish to specify
        this blas library explicitely.

	You need to load basic toolchain modules and any CUDA or OpenCL module.
]])

whatis("Version: 2.12.0")
whatis("Keywords: OpenCL, computing, numeric, linear algebra, gpu")
whatis("URL: https://github.com/clMathLibraries/clBLAS")
whatis("Description: clBLAS - a software library containing BLAS functions written in OpenCL.")

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
prereq_any("gcccuda","fosscuda","CUDA","opencl_cpu","opencl_amd")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prepend_path("CPATH", pathJoin(root, "clBLAS-2.12.0/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "clBLAS-2.12.0/lib64"))
prepend_path("LIBRARY_PATH", pathJoin(root, "clBLAS-2.12.0/lib64"))
prepend_path("PATH", pathJoin(root, "clBLAS-2.12.0/bin"))

setenv("CLBLAS_ROOT", pathJoin(root, "clBLAS-2.12.0"))
