-- *** Module definition comes here  ***

help([[
This package provides Bohrium - a runtime environment for efficiently executing
	vectorized applications using Python/NumPy, C, and C++.

	See $BH_CONFIG for details concerning default bohrium configuration.	
	Bohrium installation can be found in $BOHRIUM_ROOT.

	Use enviromental variable BH_STACK set to openmp/opencl/cuda to select
	a correct configuration stack.

	You need to load manually GCC and MPI modules. Optionally you can load
	OpenCL or CUDA accelerators. Other relevant modules will be autoloaded.

	Required: GCC/5.4.0-2.26 OpenMPI/1.10.3
	Optional (any of): opencl_cpu/16.1.1 CUDA/9.0.176-detached opencl_amd/sdk3.0
]])

whatis("Version: 0.9.2")
whatis("Keywords: utilities, computing, parallel, numeric, gpu")
whatis("URL: http://www.bh107.org")
whatis("Description: Automatic acceleration of array operations in Python/NumPy, C, and C++ targeting multi-core CPUs and GP-GPUs.")

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("FFTW/3.3.4", "Boost/1.61.0", "Python/2.7.12/maxiv/0.1", "OpenCV/3.1.0")

if isloaded("opencl_cpu/16.1.1") or isloaded("CUDA") or isloaded("opencl_amd/sdk3.0") then
	always_load("LAPACKE/3.6.1-foss2016b","clBLAS/2.12-foss2016b","clFFT/2.12.2-foss2016b")
end

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prepend_path("PATH", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/bin"))
prepend_path("CPATH", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/lib64"))
prepend_path("LIBRARY_PATH", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/lib64"))
prepend_path("PYTHONPATH", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/lib64/python2.7/site-packages"))

setenv("BH_CONFIG", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2/etc/bohrium/config.ini"))

setenv("BOHRIUM_ROOT", pathJoin(root, "pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3/bohrium-0.9.2"))
