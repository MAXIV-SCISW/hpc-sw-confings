-- *** Module definition comes here  ***

help([[
This package provides Newmat - C++ library intended for manipulation with
        a variety of types of matrices using standard matrix operations.

        Emphasis is on the kind of operations needed in statistical calculations
        such as least squares, linear equation solve and eigenvalues. 

	Required: GCC/6.4.0-2.28 OpenMPI/2.1.2
]])

whatis("Version: 1.10.4")
whatis("Keywords: matrix, computing, numeric")
whatis("URL: http://www.robertnz.net/nm_intro.htm")
whatis("Description: C++ library for manipulation with a variety of types of matrices using standard matrix operations.")

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prepend_path("PATH", pathJoin(root, "newmat-1.10.4/bin"))
prepend_path("CPATH", pathJoin(root, "newmat-1.10.4/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "newmat-1.10.4/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "newmat-1.10.4/lib"))

setenv("NEWMAT_ROOT", pathJoin(root, "newmat-1.10.4"))
