-- *** Module definition comes here  ***

help([[
This package provides clFFT.

	Use 'export BLAS=$clFFT_ROOT/lib64/libclFFT.so' if wish to specify
        this blas library explicitely.

	You need to load basic toolchain modules and any CUDA or OpenCL module.
]])

whatis("Version: 2.12.2")
whatis("Keywords: OpenCL, computing, numeric, FFT, FT, Fourier transform, gpu")
whatis("URL: https://github.com/clMathLibraries/clFFT")
whatis("Description: clFFT -  software library containing FFT functions written in OpenCL.")

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
prereq_any("gcccuda","fosscuda","CUDA","opencl_cpu","opencl_amd")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prepend_path("CPATH", pathJoin(root, "clFFT-2.12.2/include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "clFFT-2.12.2/lib64"))
prepend_path("LIBRARY_PATH", pathJoin(root, "clFFT-2.12.2/lib64"))
prepend_path("PATH", pathJoin(root, "clFFT-2.12.2/bin"))

setenv("CLFFT_ROOT", pathJoin(root, "clFFT-2.12.2"))
