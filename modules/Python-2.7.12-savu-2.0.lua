-- *** Module definition comes here  ***

help([[
This package provides Savu - a Python package to assist with the processing and
	reconstruction of parallel-beam tomography data.	
]])

whatis("Version: 2.0")
whatis("Keywords: tomography, image reconstruction, python, DLS")
whatis("URL: https://savu.readthedocs.org/en/latest/")
whatis("Description: Savu - Tomography Reconstructon Pipeline.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/5.4.0-2.26","OpenMPI/1.10.3")
always_load("Python/2.7.12/maxiv/0.2","Python/2.7.12/astra-toolbox/1.8","Python/2.7.12/tomopy/1.0.2","Python/2.7.12/xraylib/3.3.0")

prepend_path("PATH", pathJoin(root,"Python-2.7.12-savu-2.0/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.12-savu-2.0/lib/python2.7/site-packages"))

setenv("SAVU_ROOT", pathJoin(root,"Python-2.7.12-savu-2.0"))
