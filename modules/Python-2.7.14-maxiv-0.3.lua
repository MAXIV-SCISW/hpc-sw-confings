-- *** Module definition comes here  ***

help([[
This package provides several common Python packeges and applications.
	
	Namely it provedes the following modules and applications:
		ipython jupyter pyfftw pyopencl pycuda Pillow
                scikit-image dask scikit-learn pywavelets pyzmq
                numexpr futures pyopengl fabio libtiff etc.
]])

whatis("Version: 0.3")
whatis("Keywords: Python, utilities")
whatis("URL: https://wiki.maxiv.lu.se/index.php/HPC_Software")
whatis("Description: Some usefull python modules and applications")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("FFTW/3.3.7","matplotlib/2.1.2-Python-2.7.14","bitshuffle/0.3.4-h5py-2.7.1-Python-2.7.14-HDF5-1.10.1","Boost/1.66.0-Python-2.7.14","ZeroMQ/4.2.5","libjpeg-turbo/1.5.2","LibTIFF/4.0.9")

prepend_path("PATH", pathJoin(root,"Python-2.7.14-maxiv-0.3/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.14-maxiv-0.3/lib/python2.7/site-packages"))
prepend_path("CPATH", pathJoin(root,"Python-2.7.14-maxiv-0.3/include/python2.7"))

setenv("MAXIV_ROOT", pathJoin(root,"Python-2.7.14-maxiv-0.3"))

