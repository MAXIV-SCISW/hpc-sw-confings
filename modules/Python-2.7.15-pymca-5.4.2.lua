-- *** Module definition comes here  ***

help([[
This package provides PyMca - Applications and toolkit for X-ray fluorescence analysis.
	
	This package provides several utilities as pymca, pymcabatch,
pymcaroitool, edfviewer or mca2edf etc. and python PyMca5 module.
	
	Type: "pymca" in terminal to start pymca5 application.
	Use: "import PyMca5" in python to import the python module.

]])

whatis("Version: 5.4.3")
whatis("Keywords: data, browser, hdf5, fluorescence, utilities")
whatis("URL: http://pymca.sourceforge.net/")
whatis("Description: PyMca - X-ray Fluorescence Toolkit.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/7.3.0-2.30","OpenMPI/3.1.1")
always_load("Python/2.7.15/silx/0.10.1","Python/2.7.15/QtConsole/4.4.3")

prepend_path("PATH", pathJoin(root,"Python-2.7.15-pymca-5.4.2/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"Python-2.7.15-pymca-5.4.2/lib/python2.7/site-packages"))

setenv("PYMCA_ROOT", pathJoin(root,"Python-2.7.15-pymca-5.4.2"))
