-- *** Module definition comes here  ***

help([[
This package provides dxchange - data exchange supporting tomopy.
]])

whatis("Version: 0.1.5")
whatis("Keywords: Python, tomography")
whatis("URL: https://github.com/data-exchange/dxchange")
whatis("Description: Python data exchange supporting tomopy.")

local root=os.getenv("MAXIV_SW_ROOTPATH") or "/mxn/groups/pub/sw"

prereq("GCC/6.4.0-2.28","OpenMPI/2.1.2")
always_load("Python/2.7.14/maxiv/0.3","netCDF/4.6.0")

prepend_path("PATH", pathJoin(root,"/Python-2.7.14-dxchange-0.1.5/bin"))
prepend_path("PYTHONPATH", pathJoin(root,"/Python-2.7.14-dxchange-0.1.5/lib/python2.7/site-packages"))
