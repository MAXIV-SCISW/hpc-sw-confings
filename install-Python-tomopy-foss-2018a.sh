#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.14-tomopy-1.1.2

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.4.0-2.28 OpenMPI/2.1.2 Python/2.7.14/dxchange/0.1.5 imkl/2019.0.117

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- tomopy ----
python -c "import tomopy"
if [ $? -eq 0 ]
then
	echo "tomopy python module already available, skipping installation tomopy"
else
	rm -rfd tomopy
	git clone https://github.com/tomopy/tomopy.git
	CWD=$(pwd)
	cd tomopy
	git checkout 1.1.2
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/tomopy
cp modules/Python-2.7.14-tomopy-1.1.2.lua $MOD_DEST/Python/2.7.14/tomopy/1.1.2.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/tomopy/1.1.2.lua

# End
echo 'DONE'
exit 0

