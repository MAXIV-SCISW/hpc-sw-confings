#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-3.6.6-xrt-1.3.3

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use $MOD_DEST
module add opencl_cpu/16.1.1 # this must be on a separate line
module add foss/2018b gcccuda/2018b Spyder/3.3.2-Python-3.6.6 Python/3.6.6/maxiv/0.4

module list

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment


function install_module_git {
	module_name=$1
	version_tag=$2
	git_url=$3

	python -c "import $module_name"
	if [ $? -eq 0 ]
	then
		echo "$module_name python module already available, skipping installation $module_name"
	else
		CWD=$(pwd)
		cd build
		git clone $git_url
		cd $module_name; git fetch; git checkout $version_tag; git reset --hard
		# broken pip
		#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
		python setup.py install -v --prefix=$SW_DEST/$PROJECT
		cd $CWD
	fi
}

function install_module_pip {
	module_name=$1
	version=$2

	python -c "import $module_name"
	if [ $? -eq 0 ]
	then
		echo "$module_name python module already available, skipping installation $module_name"
	else
		pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT $module_name==$version
	fi
}

function install_module_tgz {
	module_name=$1
	pkg_name=$2
	src_url=$3

	python -c "import $module_name"
	if [ $? -eq 0 ]
	then
		echo "$module_name python module already available, skipping installation $module_name"
	else
		CWD=$(pwd)
		cd build
		wget -O $pkg_name.tgz $src_url
		tar -xf $pkg_name.tgz
		cd $pkg_name
		python setup.py install -v --prefix=$SW_DEST/$PROJECT
		cd $CWD
	fi
}

# --- xlwt ----
install_module_git xlwt v1.3.0 https://github.com/python-excel/xlwt.git

# --- PyOpenGL-accelerate ----
install_module_tgz OpenGL_accelerate PyOpenGL-accelerate-3.1.0 https://files.pythonhosted.org/packages/d9/74/293aa8794f2f236186d19e61c5548160bfe159c996ba01ed9144c89ee8ee/PyOpenGL-accelerate-3.1.0.tar.gz

# --- xrt ----
install_module_git xrt v1.3.3 https://github.com/kklmn/xrt.git
CWD=$(pwd)
cd $SW_DEST/$PROJECT/lib/python3.6/site-packages
unzip xrt-1.3.3-py3.6.egg
cd $CWD

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/xrt/
cp $SCDIR/modules/Python-3.6.6-xrt-1.3.3.lua $MOD_DEST/Python/3.6.6/xrt/1.3.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/xrt/1.3.3.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/xrt/1.3.3.lua

chmod o+r $MOD_DEST/Python/3.6.6/xrt/
chmod o+x $MOD_DEST/Python/3.6.6/xrt/

# End
echo 'DONE'
exit 0

