#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.15-pyfai-0.17.1

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use /mxn/groups/pub/sw/modules/maxiv
module add opencl_cpu/16.1.1 # this must be on a separate line
module add foss/2018b gcccuda/2018b Python/2.7.15/silx/0.10.1

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- pyFAI ----
python -c "import pyFAI"
if [ $? -eq 0 ]
then
	echo "pyFAI python module already available, skipping installation pyFAI"
else
	CWD=$(pwd); cd build
	#rm -rf pyFAI
	#git clone https://github.com/silx-kit/pyFAI.git
	#cd pyFAI; git checkout v0.17.1; git reset --hard
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyFAI==0.17.1
	cd $CWD
fi

# Group s-flag for module destination
chmod g+s $MOD_DEST/Python/2.7.15/pyFAI/

# Create module
mkdir -p $MOD_DEST/Python/2.7.15/pyFAI/
cp modules/Python-2.7.15-pyfai-0.17.1.lua $MOD_DEST/Python/2.7.15/pyFAI/0.17.1.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/2.7.15/pyFAI/0.17.1.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/2.7.15/pyFAI/0.17.1.lua

chmod o+r $MOD_DEST/Python/2.7.15/pyFAI/
chmod o+x $MOD_DEST/Python/2.7.15/pyFAI/

# End
echo 'DONE'
exit 0

