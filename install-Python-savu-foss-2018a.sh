#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.14-savu-2.3.1 

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages/

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.4.0-2.28 OpenMPI/2.1.2 Python/2.7.14/maxiv/0.3 PyYAML/3.12-Python-2.7.14 CUDA/9.1.85 Python/2.7.14/astra-toolbox/1.8.3 Python/2.7.14/tomopy/1.1.2 Python/2.7.14/xraylib/3.3.0

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- required python modules ---
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT gnureadline==6.3.8
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py==375.53

# --- savu ----
python -c "import savu"
if [ $? -eq -1 ]
then
	echo "savu python module already available, skipping installation savu"
else
	rm -rfd Savu-2.3.1
	rm -rfd Savu
#	git clone git@gitlab.com:tomograms/Savu.git
	git clone https://github.com/zdemat/Savu
#	wget https://github.com/DiamondLightSource/Savu/archive/v2.3.1.tar.gz -O savu-2.3.1.tar.gz
#	tar -xf savu-2.3.1.tar.gz
	CWD=$(pwd)
#	cd Savu-2.3.1
	cd Savu
	git pull
#	git checkout maxiv-v2.0
	git checkout v2.3.1
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/savu/
cp modules/Python-2.7.14-savu-2.3.1.lua $MOD_DEST/Python/2.7.14/savu/2.3.1.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/savu/2.3.1.lua

# End
echo 'DONE'
exit 0

