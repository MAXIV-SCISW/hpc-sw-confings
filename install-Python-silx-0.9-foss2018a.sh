#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.14-silx-0.9.0

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.4.0-2.28 OpenMPI/2.1.2 opencl_cpu/16.1.1 CUDA/9.1.85 Python/2.7.14/maxiv/0.3 PyQt5/5.10.1-Python-2.7.14

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment
export SPECFILE_USE_GNU_SOURCE=1 # for silx

# --- silx ----
python -c "import silx"
if [ $? -eq 0 ]
then
	echo "silx python module already available, skipping installation silx"
else
	rm -rfd silx-0.9.0
	wget https://github.com/silx-kit/silx/archive/v0.9.0.tar.gz -O silx-0.9.0.tar.gz
	tar -xf silx-0.9.0.tar.gz
	CWD=$(pwd)
	cd silx-0.9.0
        # disable silx.gui.plot.test.testPixelIntensityHistoAction causing segfault
        sed -i -e 's/testPixelIntensityHistoAction.suite()/#testPixelIntensityHistoAction.suite()/g' silx/gui/plot/test/__init__.py	
        pip install -v --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	cd $CWD
	#pip install -v --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT silx==0.9.0
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/silx/
cp modules/Python-2.7.14-silx-0.9.0.lua $MOD_DEST/Python/2.7.14/silx/0.9.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/silx/0.9.0.lua

# End
echo 'DONE'
exit 0

