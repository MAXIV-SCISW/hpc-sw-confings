#!/bin/bash

umask 002

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` == "gn0" ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/data/visitors/nanomax/20200007/2020042508/process/sw
MOD_DEST=/data/visitors/nanomax/20200007/2020042508/process/sw/modules

PROJECT=Python-3.7.4-pytorch-1.4-nm-0.1

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.7/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use $MOD_DEST
module use /sw/easybuild/modules/all/MPI/GCC/8.3.0/OpenMPI/3.1.4  # HACK
module use /mxn/groups/pub/sw/modules/maxiv  # MAX IV modules
module add fosscuda/2019b IPython/7.9.0-Python-3.7.4 scikit-image/0.16.2-Python-3.7.4 bitshuffle/0.3.5-h5py-2.10.0-Python-3.7.4-HDF5-1.10.5 OpenCV/4.2.0-Python-3.7.4 R-keras/2.2.5.0-Python-3.7.4-R-3.6.2 torchvision/0.5.0-Python-3.7.4

# Check if swadmin has disable-pip-version-check = True
VAR=`pip config get global.disable-pip-version-check`
if [ $VAR != "True" ]; then
  echo 'Error: pip upgrade is broken in this toolchain. Please disable ipi upgrade in your pip.conf'
	exit -1
fi

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- torchsummary ----
python -c "import torchsummary"
if [ $? -eq 0 ]
then
	echo "torchsummary python module already available, skipping installation torchsummary"
else
	# NOTE: the pip is broken, you should have disable-pip-version-check = True set in your pip.conf
	CWD=$(pwd)
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT torchsummary==1.5.1
	cd $CWD
fi

# --- attrdict ----
python -c "import attrdict"
if [ $? -eq 0 ]
then
	echo "attrdict python module already available, skipping installation attrdict"
else
	# NOTE: the pip is broken, you should have disable-pip-version-check = True set in your pip.conf
	CWD=$(pwd)
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT attrdict==2.0.1
	cd $CWD
fi

# --- moviepy ----
python -c "import moviepy"
if [ $? -eq 0 ]
then
	echo "moviepy python module already available, skipping installation moviepy"
else
	# NOTE: the pip is broken, you should have disable-pip-version-check = True set in your pip.conf
	CWD=$(pwd)
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT moviepy==1.0.1
	cd $CWD
fi

# --- tensorboardX ----
python -c "import tensorboardX"
if [ $? -eq 0 ]
then
	echo "tensorboardX python module already available, skipping installation tensorboardX"
else
	# NOTE: the pip is broken, you should have disable-pip-version-check = True set in your pip.conf
	CWD=$(pwd)
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT tensorboardX==2.0
	cd $CWD
fi

# Group s-flag for module destination
mkdir -p $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/
chmod g+s $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/

# Create module
cp modules/Python-3.7.4-pytorch-1.4-nm-0.1.lua $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/0.1.lua
#sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/0.1.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/0.1.lua

chmod o+r $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/
chmod o+x $MOD_DEST/Python/3.7.4/pytorch-1.4-nm/

# End
echo 'DONE'
exit 0

