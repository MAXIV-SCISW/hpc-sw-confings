#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-3.6.6-pymca-5.5.1

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use $MOD_DEST # not much needed
module add opencl_cpu/16.1.1 # this must be on a separate line
module add foss/2018b gcccuda/2018b Python/3.6.6/silx/0.11.0 Python/3.6.6/QtConsole/4.4.3

module list

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment

# --- PyOpenGL ---
python -c "import OpenGL" # it should be part of maxiv module
if [ $? -eq 0 ]
then
	echo "OpenGL python module already available, skipping installation PyOpenGL"
else
	pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl==3.1.3b2
fi
# --- fisx --- # broken pit in python3.6.6
python -c "import fisx"
if [ $? -eq 0 ]
then
	echo "fisx python module already available, skipping installation pymca"
else
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pymca==5.5.1
	CWD=$(pwd)
	cd build
	git clone https://github.com/vasole/fisx.git
	cd fisx; git fetch; git checkout v1.1.7; git reset --hard
	# broken pip, error when using --no-cache-dir, repaired in later versions
    #pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi
# --- pymca ---
python -c "import PyMca5"
if [ $? -eq 0 ]
then
	echo "pymca python module already available, skipping installation pymca"
else
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pymca==5.5.1
	CWD=$(pwd)
	cd build
	git clone https://github.com/vasole/pymca.git
	cd pymca; git fetch; git checkout v5.5.1; git reset --hard
	# broken pip, error when using --no-cache-dir, repaired in later versions
    #pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/pymca/
cp $SCDIR/modules/Python-3.6.6-pymca-5.5.1.lua $MOD_DEST/Python/3.6.6/pymca/5.5.1.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/pymca/5.5.1.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/pymca/5.5.1.lua

chmod o+r $MOD_DEST/Python/3.6.6/pymca/
chmod o+x $MOD_DEST/Python/3.6.6/pymca/

# End
echo 'DONE'
exit 0

