#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.12-pyfai-0.15.0

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 opencl_cpu/16.1.1 CUDA/9.0.176-detached Python/2.7.12/silx/0.8.0

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- pyFAI ----
python -c "import pyFAI"
if [ $? -eq 0 ]
then
	echo "pyFAI python module already available, skipping installation pyFAI"
else
	rm -rfd pyFAI-0.15.0
	#wget https://github.com/silx-kit/pyFAI/archive/v0.15.0.tar.gz -O pyfai-0.15.0.tar.gz
	#tar -xf pyfai-0.15.0.tar.gz
	#CWD=$(pwd)
	#cd pyFAI-0.15.0
        #python setup.py install --prefix=$SW_DEST/$PROJECT	
	#cd $CWD
	pip install -v --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyFAI==0.15.0
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/pyFAI/
cp modules/Python-2.7.12-pyfai-0.15.0.lua $MOD_DEST/Python/2.7.12/pyFAI/0.15.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/pyFAI/0.15.0.lua

# End
echo 'DONE'
exit 0

