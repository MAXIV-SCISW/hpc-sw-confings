#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.12-xraylib-3.3.0

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 Python/2.7.12/maxiv/0.2

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- astra ----
python -c "import xraylib"
if [ $? -eq 0 ]
then
	echo "xraylib python module already available, skipping installation xraylib"
else
	rm -rfd xraylib-3.3.0
	wget https://github.com/tschoonj/xraylib/archive/xraylib-3.3.0.tar.gz -O xraylib-3.3.0.tgz
	tar -xf xraylib-3.3.0.tgz
	CWD=$(pwd)
	mv xraylib-xraylib-3.3.0 xraylib-3.3.0
	cd xraylib-3.3.0
	autoreconf -i
	./configure --enable-python --disable-perl --disable-java --disable-fortran2003 --disable-lua --prefix=$SW_DEST/$PROJECT
	make
	make check
	make install
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/xraylib/
cp modules/Python-2.7.12-xraylib-3.3.0.lua $MOD_DEST/Python/2.7.12/xraylib/3.3.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/xraylib/3.3.0.lua

# End
echo 'DONE'
exit 0

