#!/bin/bash

umask 022

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` == gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-3.6.6-silx-0.12.0

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
module purge
module use $MOD_DEST
module add opencl_cpu/16.1.1 # this must be on a separate line
module add CUDA/9.2.88-detached foss/2018b clFFT/2.12.2-foss2018b Python/3.6.6/maxiv/0.4 PyQt5/5.11.3-Python-3.6.6

module list

# --- test fabio ----
python -c "import fabio"
if [ $? -eq 0 ]
then
	echo "we have fabio"
else
	echo "we do not have fabio, exit"
fi

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment
export SPECFILE_USE_GNU_SOURCE=1 # for silx

# --- fabio beta ---
# it is hard to stop silx to do not install this, so we do it either manually
python setup.py install -v --prefix=$SW_DEST/$PROJECT
#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio==0.9.0
CWD=$(pwd)
cd build
git clone https://github.com/silx-kit/fabio.git
cd fabio; git checkout v0.9.0; git reset --hard
python setup.py install -v --prefix=$SW_DEST/$PROJECT
cd $CWD	

# --- scikit-cuda ---
# note> we can install but this requires nvidia-driver>400 in order to run cuBLAS in gcccuda/2018b
python -c "import skcuda"
if [ $? -eq 0 ]
then
	echo "skcuda python module already available, skipping installation scikit-cuda"
else
	# broken pip, error when using --no-cache-dir, repaired in later versions
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-cuda==0.5.3
	CWD=$(pwd)
	cd build
	git clone https://github.com/lebedov/scikit-cuda.git
	cd scikit-cuda; git checkout v0.5.3; git reset --hard
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD		
fi
# --- gpyfft ---
python -c "import gpyfft"
if [ $? -eq 0 ]
then
	echo "gpyfft python module already available, skipping installation gpyfft"
else
	# pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT weakref # unnecessary for python3
	CWD=$(pwd)
	cd build
	git clone https://github.com/geggo/gpyfft.git
	# v0.7.1 requires weakref.finalize (it may be there for python3.* and python2.17)
	cd gpyfft; git checkout v0.7.1; git reset --hard
	# broken pip, error when using --no-cache-dir, repaired in later versions
    #pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi
# --- silx ----
python -c "import silx"
if [ $? -eq 0 ]
then
	echo "silx python module already available, skipping installation silx"
else
	CWD=$(pwd)
	cd build
	git clone https://github.com/silx-kit/silx.git
	cd silx; git checkout v0.12.0; git reset --hard
        # disable silx.gui.plot.test.testPixelIntensityHistoAction causing segfault
        #sed -i -e 's/testPixelIntensityHistoAction.suite()/#testPixelIntensityHistoAction.suite()/g' silx/gui/plot/test/__init__.py
	sed -i -e 's/test_suite.addTest(test_curve_fit())/#test_suite.addTest(test_curve_fit())/g' silx/math/fit/test/__init__.py
	sed -i -e 's/test_suite.addTest(test_fitmanager())/#test_suite.addTest(test_fitmanager())/g' silx/math/fit/test/__init__.py
	sed -i -e 's/mae\ <\ np.abs(input_data.max())/mae\ <\ np.abs(res_np.max())/g' silx/math/fft/test/test_fft.py
	# otherwise installs fabio-0.9.0b0 (gave up)
	#sed -i -e 's/fabio\ >=\ 0.7/#fabio/g' requirements.txt
	# broken pip
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	python setup.py install -v --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/silx/
cp $SCDIR/modules/Python-3.6.6-silx-0.12.0.lua $MOD_DEST/Python/3.6.6/silx/0.12.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/silx/0.12.0.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/silx/0.12.0.lua

chmod o+r $MOD_DEST/Python/3.6.6/silx/
chmod o+x $MOD_DEST/Python/3.6.6/silx/

# End
echo 'DONE'
exit 0
