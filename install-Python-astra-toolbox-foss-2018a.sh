#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.14-astra-toolbox-1.8.3 

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/python:$PYTHONPATH
#export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH
export LD_RUN_PATH=$SW_DEST/$PROJECT/lib:$LD_RUN_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
# matlab first in order to shadow its old *.so libs
module load matlab/R2018a GCC/6.4.0-2.28 OpenMPI/2.1.2 Boost/1.66.0-Python-2.7.14 Python/2.7.14/maxiv/0.3 CUDA/9.1.85

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- astra ----
python -c "import astra"
if [ $? -eq 0 ]
then
	echo "astra python module already available, skipping installation astra"
else
	rm -rfd astra-toolbox-1.8.3
	wget https://github.com/astra-toolbox/astra-toolbox/archive/v1.8.3.tar.gz -O astra-toolbox-1.8.3.tgz
	tar -xf astra-toolbox-1.8.3.tgz
	CWD=$(pwd)
	cd astra-toolbox-1.8.3/build/linux
	./autogen.sh   # when building a git version
	# add -R2017b MEX FLAG (does not work)
	#sed -i -e 's/MEXFLAGS\ \ \=\ -cxx/MEXFLAGS\ \ \= -R2017b -cxx/g' Makefile.in
	# we have to try the workaround suggested by 5ht2: https://github.com/astra-toolbox/astra-toolbox/issues/149
	sed -i -e 's/\#define\ USE_MATLAB_UNDOCUMENTED/\/\/\#define\ USE_MATLAB_UNDOCUMENTED/g' ../../matlab/mex/mexDataManagerHelpFunctions.cpp
	./configure --with-python \
              --with-matlab=/sw/pkg/matlab/R2018a \
              --with-install-type=prefix \
              --prefix=$SW_DEST/$PROJECT \
              --exec-prefix=$SW_DEST/$PROJECT \
	      --with-cuda=/sw/easybuild/software/Compiler/GCC/6.4.0-2.28/CUDA/9.1.85/ \
              --with-cuda-compute=37,60,70
	make
  	make install-libraries 
	make install-python
	make install-matlab
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/astra-toolbox/
cp modules/Python-2.7.14-astra-toolbox-1.8.3.lua $MOD_DEST/Python/2.7.14/astra-toolbox/1.8.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/astra-toolbox/1.8.3.lua

# End
echo 'DONE'
exit 0

