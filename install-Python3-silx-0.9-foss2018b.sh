#!/bin/bash

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ `hostname` == gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-3.6.6-silx-0.9.0

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages

# Start installing
module purge
module use $MOD_DEST
module add opencl_cpu/16.1.1 # this must be on a separate line
module add foss/2018b gcccuda/2018b Python/3.6.6/maxiv/0.4 PyQt5/5.11.3-Python-3.6.6

module list

# --- test fabio ----
python -c "import fabio"
if [ $? -eq 0 ]
then
	echo "we have fabio"
else
	echo "we do not have fabio, exit"
fi

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment
export SPECFILE_USE_GNU_SOURCE=1 # for silx

# --- silx ----
python -c "import silx"
if [ $? -eq 0 ]
then
	echo "silx python module already available, skipping installation silx"
else
	rm -rfd silx-0.9.0
	wget https://github.com/silx-kit/silx/archive/v0.9.0.tar.gz -O silx-0.9.0.tar.gz
	tar -xf silx-0.9.0.tar.gz
	CWD=$(pwd)
	cd silx-0.9.0
        # disable silx.gui.plot.test.testPixelIntensityHistoAction causing segfault
        sed -i -e 's/.\/fabio-0.8.0-py3.6-linux-x86_64.egg/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1\/Python-3.6.6-maxiv-0.4\/lib\/python3.6\/site-packages\/fabio-0.8.0-py3.6-linux-x86_64.egg/g' $SW_DEST/$PROJECT/lib/python3.6/site-packages/easy-install.pth
        #pip3 install -v --ignore-requires-python --disable-pip-version-check --no-clean --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
	python setup.py build --openmp --force-cython
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
	#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT silx==0.9.0
	# remove (broken) fabio	installed with this even not requied
        #rm -rfd $SW_DEST/$PROJECT/lib/python3.6/site-packages/fabio*
        #sed -i -e 's/.\/fabio-0.8.0-py3.6-linux-x86_64.egg/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1\/Python-3.6.6-maxiv-0.4\/lib\/python3.6\/site-packages\/fabio-0.8.0-py3.6-linux-x86_64.egg/g' $SW_DEST/$PROJECT/lib/python3.6/site-packages/easy-install.pth
	# note: giving up, results of silx installations is random
fi

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/silx/
cp $SCDIR/modules/Python-3.6.6-silx-0.9.0.lua $MOD_DEST/Python/3.6.6/silx/0.9.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/silx/0.9.0.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/silx/0.9.0.lua

chmod o+r $MOD_DEST/Python/3.6.6/silx/
chmod o+x $MOD_DEST/Python/3.6.6/silx/

# End
echo 'DONE'
exit 0

