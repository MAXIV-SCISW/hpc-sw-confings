#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/5.4.0-2.26/OpenMPI/1.10.3

PROJECT=Python-2.7.12-maxiv-0.2

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
#module load GCC/5.4.0-2.26  OpenMPI/1.10.3 FFTW/3.3.4 matplotlib/1.5.3-Python-2.7.12 h5py/2.6.0-Python-2.7.12-HDF5-1.8.17 Boost/1.61.0 matlab/R2017a CMake/3.6.1 ZeroMQ/4.2.0 PyZMQ/16.0.2-Python-2.7.12-zmq4 opencl_cpu/16.1.1 CUDA/8.0.44-GCC-5.4.0-2.26 libjpeg-turbo/1.5.1
module load GCC/5.4.0-2.26  OpenMPI/1.10.3 FFTW/3.3.4 matplotlib/1.5.3-Python-2.7.12 h5py/2.6.0-Python-2.7.12-HDF5-1.8.17 Boost/1.61.0 matlab/R2017a CMake/3.6.1 ZeroMQ/4.2.0 PyZMQ/16.0.2-Python-2.7.12-zmq4 CUDA/8.0.44-GCC-5.4.0-2.26 libjpeg-turbo/1.5.1 LibTIFF/4.0.6

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH # results in broken pyopencl

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT ipython
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT jupyter
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl
# decorator for sci-kit image
#pip install --no-cache-dir --no-binary :all: --force --ignore-installed --prefix=$SW_DEST/$PROJECT decorator
CWD=$(pwd)
wget https://pypi.python.org/packages/bb/e0/f6e41e9091e130bf16d4437dabbac3993908e4d6485ecbc985ef1352db94/decorator-4.1.2.tar.gz#md5=a0f7f4fe00ae2dde93494d90c192cf8c -O decorator-4.1.2.tar.gz
tar -xf decorator-4.1.2.tar.gz
cd decorator-4.1.2
python setup.py install --prefix=$SW_DEST/$PROJECT
cd $CWD
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-image
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pywavelets
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT futures
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pycuda
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff=0.4.2

# savu_magic
cp ./savu_magick $SW_DEST/$PROJECT/bin/

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/maxiv
cp modules/Python-2.7.12-maxiv-0.2.lua $MOD_DEST/Python/2.7.12/maxiv/0.2.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/maxiv/0.2.lua

# End
echo 'DONE'
exit 0
