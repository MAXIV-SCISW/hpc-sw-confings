#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.3.0-2.27-OpenMPI-2.0.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.13-dxchange-0.1.3

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.3.0-2.27 OpenMPI/2.0.2 Python/2.7.13/maxiv/0.1 netCDF/4.4.1.1-HDF5-1.10.1

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- tifffile ---
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT tifffile
# --- spefile ----
python -c "import spefile"
if [ $? -eq 0 ]
then
	echo "spefile python module already available, skipping installation spefile"
else
	rm -rfd spefile-feedstock
	git clone https://github.com/conda-forge/spefile-feedstock.git
	CWD=$(pwd)
	cd spefile-feedstock/recipe/src
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi
# --- dxfile ----
python -c "import dxfile"
if [ $? -eq 0 ]
then
	echo "dxfile python module already available, skipping installation dxfile"
else
	rm -rfd dxfile
	git clone https://github.com/data-exchange/dxfile.git
	CWD=$(pwd)
	cd dxfile
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi
# --- edffile ----
python -c "import EdfFile"
if [ $? -eq 0 ]
then
	echo "edffile python module already available, skipping installation edffile"
else
	rm -rfd edffile-feedstock
	git clone https://github.com/conda-forge/edffile-feedstock.git
	CWD=$(pwd)
	cd edffile-feedstock/recipe/src
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi
# --- netcdf4 ---
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT netcdf4
# --- dxchange ----
python -c "import dxchange"
if [ $? -eq 0 ]
then
	echo "dxchange python module already available, skipping installation dxchange"
else
	#rm -rfd dxchange
	#git clone https://github.com/data-exchange/dxchange.git
	#CWD=$(pwd)
	#cd dxchange
	wget https://github.com/data-exchange/dxchange/archive/v0.1.3.tar.gz -O dxchange-0.1.3.tgz
	tar -xf dxchange-0.1.3.tgz
	cd dxchange-0.1.3
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.13/dxchange
cp modules/Python-2.7.13-dxchange-0.1.3.lua $MOD_DEST/Python/2.7.13/dxchange/0.1.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.3.0-2.27-OpenMPI-2.0.2/g' $MOD_DEST/Python/2.7.13/dxchange/0.1.3.lua

# End
echo 'DONE'
exit 0

