#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/6.4.0-2.28/OpenMPI/2.1.2

PROJECT=Python-2.7.14-maxiv-0.4

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module purge
module use $MOD_DEST # not much needed
module add opencl_cpu/16.1.1 # this must be on a separate line
module add matlab/R2018a # rather on a separate line
module add foss/2018a gcccuda/2018a matplotlib/2.1.2-Python-2.7.14 bitshuffle/0.3.4-h5py-2.7.1-Python-2.7.14-HDF5-1.10.1 Boost/1.66.0-Python-2.7.14  ZeroMQ/4.2.5 libjpeg-turbo/1.5.2 LibTIFF/4.0.9 IPython/5.7.0-Python-2.7.14 CMake/3.10.2

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# setuptools, testpath, ipython, jupyter, tornado, pyzmq should come from IPython module now, note> Python3 should take also scikit-image and scikit-learn

# new path with easy_install (just in case)
export PATH=$SW_DEST/$PROJECT/bin:$PATH

pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw==0.11.1
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pybind11==2.2.4
# pybind11 deployed some CPP headers
export CPATH=$SW_DEST/$PROJECT/include/python2.7:$CPATH
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pycuda==2018.1.1 # appdirs, pytools, py, attrs, more-itertools, atomicwrites, pluggy, pytest*, mako
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl==2018.2.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pywavelets==1.0.1 # Python3: in scikit-image
# decorator for sci-kit image
pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT decorator==4.3.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-image==0.14.2 # decorator*, pytest*, networkx, pillow, toolz, dask, cloudpickle, note: for Python3 it is in modules
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-learn==0.20.2 # note: for Python3 it is in modules
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT futures==3.2.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr==2.6.9
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio==0.8.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama==0.4.1
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy==2.0.11 # pluggy, pytest*
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pytest==4.2.0 # for scikit-image 4.2.0 and astropy 3.6.4
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff==0.4.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py==375.53
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl==3.1.0

# savu_magic
cp ./savu_magick $SW_DEST/$PROJECT/bin/
cp ./plot_slice-foss-2018a $SW_DEST/$PROJECT/bin/plot_slice

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/maxiv
cp modules/Python-2.7.14-maxiv-0.4.lua $MOD_DEST/Python/2.7.14/maxiv/0.4.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/maxiv/0.4.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/2.7.14/maxiv/0.4.lua

chmod o+r $MOD_DEST/Python/2.7.14/maxiv/
chmod o+x $MOD_DEST/Python/2.7.14/maxiv/

# End
echo 'DONE'
exit 0

