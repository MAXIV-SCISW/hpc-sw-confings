#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

YYYY=2018; MM=10; DD=19

PROJECT=Python-2.7.12-NanoPeakCell-0.3.3-$YYYY$MM$DD

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 opencl_cpu/16.1.1 CUDA/9.0.176-detached cctbx/20180528-Python-2.7.12 PyQt/4.12-Python-2.7.12 Python/2.7.12/pyFAI/0.15.0 CMake/3.12.0

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- Installation ----
python -c "import NPC"
if [ $? -eq 0 ]
then
	echo "NPC python module already available, skipping installation NanoPeakCell"
else
	# --- Dependencies ----
	pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT future==0.16.0
	pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT lz4==1.1.0
	pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyqtgraph==0.10.0
	# --- NanoPeakCell ----
	rm -rfd NanoPeakCell
	CWD=$(pwd)
	git clone https://github.com/coquellen/NanoPeakCell.git
	cd NanoPeakCell
	git checkout master
        python setup.py install --prefix=$SW_DEST/$PROJECT	
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/NanoPeakCell
cp modules/Python-2.7.12-npc-0.3.3-YYYYMMDD.lua $MOD_DEST/Python/2.7.12/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua

sed -i -e 's/YYYY/'$YYYY'/g' $MOD_DEST/Python/2.7.12/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/MM/'$MM'/g' $MOD_DEST/Python/2.7.12/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua
sed -i -e 's/DD/'$DD'/g' $MOD_DEST/Python/2.7.12/NanoPeakCell/0.3.3-$YYYY$MM$DD.lua

# End
echo 'DONE'
exit 0

