#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/5.4.0-2.26/OpenMPI/1.10.3

PROJECT=Python-2.7.12-maxiv-0.3

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use $MOD_DEST
module load matlab/R2017a GCC/5.4.0-2.26 OpenMPI/1.10.3 opencl_cpu/16.1.1 CUDA/9.0.176-detached FFTW/3.3.4 matplotlib/1.5.3-Python-2.7.12 bitshuffle/0.3.2-h5py-2.6.0-Python-2.7.12-HDF5-1.10.0-patch1 Boost/1.67.0-Python-2.7.12 CMake/3.12.0 ZeroMQ/4.2.5-foss2016b libjpeg-turbo/1.5.1 LibTIFF/4.0.6

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH # results in broken pyopencl

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# pyzmq for jupyter
pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT pyzmq==17.1.2 # upgrade/new(ZeroMQ/4.2.5) for jupyter
#CWD=$(pwd)
#wget https://files.pythonhosted.org/packages/b9/6a/bc9277b78f5c3236e36b8c16f4d2701a7fd4fa2eb697159d3e0a3a991573/pyzmq-17.1.2.tar.gz -O pyzmq-17.1.2.tar.gz
#tar -xf pyzmq-17.1.2.tar.gz
#cd pyzmq-17.1.2
#python setup.py install --prefix=$SW_DEST/$PROJECT
#cd $CWD
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT ipython==5.7.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT jupyter==1.0.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw==0.10.4
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pybind11==2.2.4
# pybind11 deployed some CPP headers
export CPATH=$SW_DEST/$PROJECT/include/python2.7:$CPATH
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pycuda==2018.1.1 # installs mako
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl==2018.2.1
# decorator for sci-kit image
#pip install --no-cache-dir --no-binary :all: --force --ignore-installed --prefix=$SW_DEST/$PROJECT decorator
CWD=$(pwd)
wget https://pypi.python.org/packages/bb/e0/f6e41e9091e130bf16d4437dabbac3993908e4d6485ecbc985ef1352db94/decorator-4.1.2.tar.gz#md5=a0f7f4fe00ae2dde93494d90c192cf8c -O decorator-4.1.2.tar.gz
tar -xf decorator-4.1.2.tar.gz
cd decorator-4.1.2
python setup.py install --prefix=$SW_DEST/$PROJECT
cd $CWD
#pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT Pillow==5.3.0 # upgrade for scikit-learn
CWD=$(pwd)
wget https://files.pythonhosted.org/packages/1b/e1/1118d60e9946e4e77872b69c58bc2f28448ec02c99a2ce456cd1a272c5fd/Pillow-5.3.0.tar.gz -O Pillow-5.3.0.tar.gz
tar -xf Pillow-5.3.0.tar.gz
cd Pillow-5.3.0
python setup.py install --prefix=$SW_DEST/$PROJECT
cd $CWD
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pywavelets==0.5.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT futures==3.2.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr==2.6.8
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-image==0.14.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-learn==0.19.1
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio==0.8.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama==0.3.9
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy==2.0.8
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff==0.4.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py==375.53
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl==3.1.0

# savu_magic
cp ./savu_magick $SW_DEST/$PROJECT/bin/
cp ./plot_slice-foss-2016b $SW_DEST/$PROJECT/bin/plot_slice

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/maxiv
cp modules/Python-2.7.12-maxiv-0.3.lua $MOD_DEST/Python/2.7.12/maxiv/0.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/maxiv/0.3.lua

# End
echo 'DONE'
exit 0
