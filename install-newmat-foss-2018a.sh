#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=newmat-1.10.4

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in LIBRARY_PATH and that it exists
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

# Start installing
module purge
module use /mxn/groups/pub/sw/modules/maxiv
module add GCC/6.4.0-2.28 OpenMPI/2.1.2 Automake/1.15.1 libtool/2.4.6

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- newmat ----
CWD=$(pwd)
mkdir -p $CWD/nwBuild
rm -rfd $CWD/nwBuild/*
cd $CWD/nwBuild

wget http://deb.debian.org/debian/pool/main/n/newmat/newmat_1.10.4-8.debian.tar.xz
wget http://deb.debian.org/debian/pool/main/n/newmat/newmat_1.10.4.orig.tar.gz
tar -xf newmat_1.10.4.orig.tar.gz
tar -xf newmat_1.10.4-8.debian.tar.xz

cd newmat-1.10.4

patch < ../debian/patches/newmat-1.10.4-msvc.diff
patch < ../debian/patches/newmat-1.10.4-libtool.diff
patch < ../debian/patches/newmat-1.10.4-config.diff

aclocal --force
autoconf
libtoolize
autoreconf -i
automake --add-missing --foreign

./configure --prefix=$SW_DEST/$PROJECT

rm - rfd $SW_DEST/$PROJECT
make install

# rename files in bin
echo "Renaming files in "$SW_DEST/$PROJECT"/bin"
cd $SW_DEST/$PROJECT/bin
for filename in *; do
    echo $filename" -> nm-"$filename 
    mv $filename nm-$filename
done

cd $CWD

# Create module
mkdir -p $MOD_DEST/newmat
cp modules/newmat-1.10.4-foss2018a.lua $MOD_DEST/newmat/1.10.4-foss2018a.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/newmat/1.10.4-foss2018a.lua

# End
echo 'DONE'
exit 0

