#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/6.4.0-2.28/OpenMPI/2.1.2

PROJECT=Python-2.7.14-maxiv-0.3

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module purge
module use $MOD_DEST
module load matlab/R2018a GCC/6.4.0-2.28 opencl_cpu/16.1.1 CUDA/9.1.85 OpenMPI/2.1.2 FFTW/3.3.7 matplotlib/2.1.2-Python-2.7.14 bitshuffle/0.3.4-h5py-2.7.1-Python-2.7.14-HDF5-1.10.1 Boost/1.66.0-Python-2.7.14 CMake/3.10.1 ZeroMQ/4.2.5 libjpeg-turbo/1.5.2 LibTIFF/4.0.9

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# newer setuptools needed anyway for testpath in jupyter
pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT setuptools==39.2.0
# new path with easy_install
export PATH=$SW_DEST/$PROJECT/bin:$PATH

pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT ipython==5.7.0
# troubles with testpath
CWD=$(pwd)
wget https://files.pythonhosted.org/packages/a6/f9/08ff55ac636b40a2b542988483f50af26d7116ad45808ab1758739c9c63d/testpath-0.4.1.tar.gz
tar --warning=no-timestamp -xf testpath-0.4.1.tar.gz
cd testpath-0.4.1
python setup.py install --prefix=$SW_DEST/$PROJECT
cd $CWD
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT jupyter==1.0.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw==0.10.4
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pybind11==2.2.4
# pybind11 deployed some CPP headers
export CPATH=$SW_DEST/$PROJECT/include/python2.7:$CPATH
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pycuda==2018.1.1 # installs mako
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl==2018.2.1
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pytest==3.6.2
# decorator for sci-kit image
pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT decorator==4.1.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pywavelets==0.5.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-image==0.14.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-learn==0.19.1
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT futures==3.2.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr==2.6.8
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio==0.8.0
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama==0.3.9
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy==2.0.8
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff==0.4.2
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT nvidia-ml-py==375.53
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopengl==3.1.0

# savu_magic
cp ./savu_magick $SW_DEST/$PROJECT/bin/
cp ./plot_slice-foss-2018a $SW_DEST/$PROJECT/bin/plot_slice

# Create module
mkdir -p $MOD_DEST/Python/2.7.14/maxiv
cp modules/Python-2.7.14-maxiv-0.3.lua $MOD_DEST/Python/2.7.14/maxiv/0.3.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Python/2.7.14/maxiv/0.3.lua

# End
echo 'DONE'
exit 0

