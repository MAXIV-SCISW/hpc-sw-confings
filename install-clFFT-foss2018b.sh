#!/bin/bash

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=clFFT-2.12.2

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Clean all installation
rm -rfd $SW_DEST/$PROJECT/*

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
#export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module purge
module use $MOD_DEST # hopefully not used at all
module add opencl_cpu/16.1.1 # this must be on a separate line
module add foss/2018b gcccuda/2018b Boost/1.68.0-Python-2.7.15 CMake/3.12.1

module list

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment

if [ 1 -eq 1 ]; then
	# clFFT		
	rm -rfd clFFT
	git clone https://github.com/clMathLibraries/clFFT.git
	CWD=$(pwd)
	cd clFFT;
	git checkout v2.12.2
	mkdir -p src/build; cd src/build
	export CC=$(which gcc)
	export CMAKE_INCLUDE_PATH=$CPATH
	export CMAKE_LIBRARY_PATH=$LIBRARY_PATH

	cmake .. -DBUILD_SHARED_LIBS=ON -DBUILD_TEST=OFF -DCMAKE_INSTALL_PREFIX=$SW_DEST/$PROJECT
	make install

	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/clFFT/
cp modules/clFFT-2.12.2-foss2018b.lua $MOD_DEST/clFFT/2.12.2.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/clFFT/2.12.2.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/clFFT/2.12.2.lua

chmod o+r $MOD_DEST/clFFT
chmod o+x $MOD_DEST/clFFT

# End
echo 'DONE'
exit 0

