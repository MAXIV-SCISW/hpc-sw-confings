#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.3.0-2.27-OpenMPI-2.0.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/6.3.0-2.27/OpenMPI/2.0.2

PROJECT=Python-2.7.13-maxiv-0.1

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.3.0-2.27 OpenMPI/2.0.2 FFTW/3.3.6 matplotlib/2.0.2-Python-2.7.13 h5py/2.7.0-Python-2.7.13-HDF5-1.10.1 Boost/1.63.0 matlab/R2017a CMake/3.8.2 ZeroMQ/4.2.2 opencl_cpu/16.1.1 libjpeg-turbo/1.5.1 LibTIFF/4.0.9

# OpenCL headers (missing in opencl_cpu/16.1.1)
export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT ipython
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT jupyter
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyfftw
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl
# decorator for sci-kit image
pip install --no-cache-dir --no-binary :all: --ignore-installed --prefix=$SW_DEST/$PROJECT decorator
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-image
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT pywavelets
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT futures
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT numexpr
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT fabio
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT colorama
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT astropy
pip install --no-cache-dir --no-binary :all: --prefix=$SW_DEST/$PROJECT libtiff==0.4.2

# savu_magic
cp ./savu_magick $SW_DEST/$PROJECT/bin/

# Create module
mkdir -p $MOD_DEST/Python/2.7.13/maxiv
cp modules/Python-2.7.12-maxiv-0.1.lua $MOD_DEST/Python/2.7.12/maxiv/0.1.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.3.0-2.27-OpenMPI-2.0.2/g' $MOD_DEST/Python/2.7.13/maxiv/0.1.lua

# End
echo 'DONE'
exit 0

