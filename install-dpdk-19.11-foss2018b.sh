#!/bin/bash

#set -o nounset

# this script can be run only in fpga-0 node
if [[ `hostname` != fpga-0 ]];
then
	echo "this script can be run only in fpga-0 node"
exit -1;
fi

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/7.3.0-2.30/OpenMPI/3.1.1

PROJECT=Python-3.6.6-dpdk-19.11

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
#source /home/fpga/sourceme_for_modules.sh
module purge
#module use $MOD_DEST # nod needed
module add foss/2018b Python/3.6.6 CMake/3.12.1 Automake/1.16.1

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# new path with easy_install (just in case)
export PATH=$SW_DEST/$PROJECT/bin:$PATH

# --- Python build dependencies ---
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT scikit-build==0.10.0
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT meson==0.53.0
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT ninja==1.9.0

# --- DPDK ----
CWD=$(pwd)
mkdir -p build; cd build
wget http://fast.dpdk.org/rel/dpdk-19.11.tar.xz -O dpdk-19.11.tar.xz
rm -rf dpdk-19.11
tar -xf dpdk-19.11.tar.xz
cd dpdk-19.11

make config T=x86_64-native-linuxapp-gcc DESTDIR=$SW_DEST/$PROJECT
sed -ri 's,(PMD_PCAP=).*,\1y,' build/.config
make
make install DESTDIR=$SW_DEST/$PROJECT

# Create module
mkdir -p $MOD_DEST/dpdk
cp $SCDIR/modules/dpdk-19.11.lua $MOD_DEST/dpdk/19.11.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/dpdk/19.11.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/dpdk/19.11.lua

chmod o+r $MOD_DEST/dpdk/
chmod o+x $MOD_DEST/dpdk/

# Note: some files (e.g. sbin directory or kernels in $DPDK_ROOT/lib/modules/3.10.0-693.17.1.el7.x86_64/extra/dpdk should have restricted file permissions)
echo 'If neede, please install the kernel modules from $DPDK_ROOT/lib/modules'

# End
echo 'DONE'
exit 0

