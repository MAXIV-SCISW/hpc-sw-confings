#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=bohrium-0.9.2

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Check if swadmin does not have ~/.bohrium/config.ini directory
FILE=~/.bohrium/config.ini
if [ -f "$FILE" ]; then
  echo 'Error: You have file '$FILE', there is a risk a bad config file will be generated.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib64/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib64:$LD_LIBRARY_PATH
export LD_RUN_PATH=$SW_DEST/$PROJECT/lib64:$LD_RUN_PATH
export PATH=$SW_DEST/$PROJECT/bin:$PATH

#mkdir -p $SW_DEST/$PROJECT/lib64/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
# matlab first in order to shadow its old *.so libs
module load matlab/R2018a GCC/5.4.0-2.26 OpenMPI/1.10.3 libsigsegv/2.11 CUDA/9.0.176-detached opencl_amd/sdk3.0 FFTW/3.3.4 Boost/1.61.0 Python/2.7.12/maxiv/0.2 OpenCV/3.1.0 LAPACKE/3.7.1-foss2016b clBLAS/2.12-foss2016b clFFT/2.12.2-foss2016b
module load CMake/3.12.0

# Setting up environment
export CC=$(which mpicc)
export CXX=$(which mpic++)
export CMAKE_INCLUDE_PATH=$CPATH
export CMAKE_LIBRARY_PATH=$LD_LIBRARY_PATH

export LIBSIGSEGV_ROOT=$EBROOTLIBSIGSEGV

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- bohrium ----
python -c "import bohrium"
if [ $? -eq 0 ]
then
	echo "bohrium python module already available, skipping installation astra"
else
	rm -rfd bohrium-0.9.2
	wget https://github.com/bh107/bohrium/archive/v0.9.2.tar.gz -O bohrium-0.9.2.tar.gz
	tar -xf bohrium-0.9.2.tar.gz
	CWD=$(pwd)
	cd bohrium-0.9.2
	mkdir build; cd build
	cmake .. -DCMAKE_INSTALL_PREFIX=$SW_DEST/$PROJECT
	make
	make install
	cd $CWD

	# Adjust and copy generated configuration file
	sed -i -e 's/\/home\/$(whoami)/\/mxn\/groups\/pub/g' /home/$(whoami)/.bohrium/config.ini
	mkdir -p $SW_DEST/$PROJECT/etc/bohrium
	cp /home/$(whoami)/.bohrium/config.ini $SW_DEST/$PROJECT/etc/bohrium
fi

# Create module
mkdir -p $MOD_DEST/bohrium
cp modules/bohrium-0.9.2.lua $MOD_DEST/bohrium/0.9.2.lua
sed -i -e 's/pkg\/GCC-5.4.0-2.26-OpenMPI-10.1.3/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/bohrium/0.9.2.lua

# End
echo 'DONE'
exit 0

