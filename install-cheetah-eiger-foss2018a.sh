#!/bin/bash

if [ `hostname` = gn0 ];
then
    # disable openib on the ethernet interface mlx5_1
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

export OMPI_MCA_mpi_warn_on_fork=0

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.4.0-2.28-OpenMPI-2.1.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Cheetah-eiger-0.1.0-experimental

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Clean all installation
rm -rfd $SW_DEST/$PROJECT/*

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Ensure destiantion is in PYTHONPATH and that it exists
#export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

#mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module purge
module use $MOD_DEST
module add foss/2018a bitshuffle/0.3.5-h5py-2.8.0-Python-2.7.14-HDF5-1.10.2-ts-foss2018a Boost/1.66.0-Python-2.7.14 CMake/3.10.2

module list

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# Setting up environment

if [ 1 -eq 1 ]; then
# Cheetah
rm -rfd cheetah
git clone https://github.com/biochem-fan/cheetah.git
CWD=$(pwd)
cd cheetah;
git checkout eiger
mkdir -p build; cd build
export CC=$(which gcc)
export CMAKE_INCLUDE_PATH=$CPATH
export CMAKE_LIBRARY_PATH=$LIBRARY_PATH

cmake .. -DBUILD_CHEETAH_EIGER=ON -DCMAKE_INSTALL_PREFIX=$SW_DEST/$PROJECT

# a very dirty way of correcting missing hdf5 libs for linking cheetah-eiger
sed -i '1 s/$/ -lhdf5 -lhdf5_hl/' ./source/cheetah-eiger/CMakeFiles/cheetah-eiger.dir/link.txt

make install

cd $CWD
fi

# copy cheetah.ini template
# TODO

# Create module
mkdir -p $MOD_DEST/Cheetah/
cp modules/Cheetah-0.1.0-experimental.lua $MOD_DEST/Cheetah/0.1.0-experimental.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.4.0-2.28-OpenMPI-2.1.2/g' $MOD_DEST/Cheetah/0.1.0-experimental.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Cheetah/0.1.0-experimental.lua

chmod o+r $MOD_DEST/Cheetah
chmod o+x $MOD_DEST/Cheetah

# End
echo 'DONE'
exit 0

