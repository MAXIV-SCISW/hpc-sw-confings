#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-6.3.0-2.27-OpenMPI-2.0.2
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.13-savu-2.0 

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$SW_DEST/$PROJECT/lib:$LD_LIBRARY_PATH

mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages/

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/6.3.0-2.27 OpenMPI/2.0.2 Python/2.7.13/maxiv/0.1 Python/2.7.13/astra-toolbox/1.8 Python/2.7.13/tomopy/1.0.2 Python/2.7.13/xraylib/3.3.0

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- savu ----
python -c "import savu"
if [ $? -eq -1 ]
then
	echo "savu python module already available, skipping installation savu"
else
#	rm -rfd Savu-2.0
	rm -rfd Savu
	git clone git@gitlab.com:tomograms/Savu.git
#	wget https://github.com/DiamondLightSource/Savu/archive/v2.0.tar.gz -O savu-2.0.tgz
#	tar -xf savu-2.0.tgz
	CWD=$(pwd)
#	cd Savu-2.0
	cd Savu
	git pull
	git checkout maxiv-v2.0
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.13/savu/
cp modules/Python-2.7.13-savu-2.0.lua $MOD_DEST/Python/2.7.13/savu/2.0.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-6.3.0-2.27-OpenMPI-2.0.2/g' $MOD_DEST/Python/2.7.13/savu/2.0.lua

# End
echo 'DONE'
exit 0

