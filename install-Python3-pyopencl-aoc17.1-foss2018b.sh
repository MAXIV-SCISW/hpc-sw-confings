#!/bin/bash

#set -o nounset

# this script can be run only in fpga-0 node
if [[ `hostname` != fpga-0 ]];
then
    echo "this script can be run only in fpga-0 node"
	exit -1;
fi

export SCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-7.3.0-2.30-OpenMPI-3.1.1
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv
TOOLCHAIN_ROOT=/sw/easybuild/software/MPI/GCC/7.3.0-2.30/OpenMPI/3.1.1

PROJECT=Python-3.6.6-pyopencl-aoc17.1

# Check if swadmin does not have ~/.local/lib directory
DIRECTORY=~/.local/lib
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Group s-flag for destination
mkdir -p $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;

# Start installing
source /home/fpga/sourceme_for_modules.sh
module purge
module use $MOD_DEST # nod needed
module add foss/2018b Python/3.6.6 fpga-sdk/intel/17.1 CMake/3.12.1

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python3.6/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python3.6/site-packages

# OpenCL headers (missing in opencl_cpu/16.1.1)
#export CPATH=/mxn/groups/pub/sw/local/AMDAPPSDK-3.0/include:$CPATH

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# setuptools, testpath, ipython, jupyter, tornado, pyzmq, scikit-image, scikit-learn should come from IPython module now

# new path with easy_install (just in case)
export PATH=$SW_DEST/$PROJECT/bin:$PATH

# in sourceme_for_fpga.sh
#export CPATH=$ALTERAOCLSDKROOT/host/include:$CPATH
#export LIBRARY_PATH=$ALTERAOCLSDKROOT/host/linux64/lib:$LIBRARY_PATH

function install_module_git {
	module_name=$1
	version_tag=$2
	git_url=$3

	python -c "import $module_name"
	if [ $? -eq 0 ]
	then
		echo "$module_name python module already available, skipping installation $module_name"
	else
		CWD=$(pwd)
		cd build
		git clone $git_url
		cd $module_name; git fetch; git checkout $version_tag; git reset --hard
		# broken pip
		#pip install -v --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT .
		python setup.py sdist
		pip install -v --prefix=$SW_DEST/$PROJECT dist/pyopencl-2018.2.2.tar.gz
		cd $CWD
	fi
}

pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pybind11==2.2.4
# pybind11 deployed some CPP headers
export CPATH=$SW_DEST/$PROJECT/include/python3.6m:$CPATH
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT appdirs==1.4.3
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pytools==2019.1.1
pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT mako==1.0.7
#pip install --no-cache-dir --no-build-isolation --no-binary :all: --prefix=$SW_DEST/$PROJECT pyopencl==2018.2.2 # appdirs, pytools
install_module_git pyopencl 2018.2.2-aoc17.1 https://github.com/zdemat/pyopencl.git

# Create module
mkdir -p $MOD_DEST/Python/3.6.6/pyopencl
cp $SCDIR/modules/Python-3.6.6-pyopencl-aoc17.1.lua $MOD_DEST/Python/3.6.6/pyopencl/aoc17.1.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-7.3.0-2.30-OpenMPI-3.1.1/g' $MOD_DEST/Python/3.6.6/pyopencl/aoc17.1.lua

# File permissions
find $SW_DEST/$PROJECT -type d -exec chmod g+s {} \;
chmod -R o+r $SW_DEST/$PROJECT
find $SW_DEST/$PROJECT -type d -exec chmod o+x {} \;

chmod o+r $MOD_DEST/Python/3.6.6/pyopencl/aoc17.1.lua

chmod o+r $MOD_DEST/Python/3.6.6/pyopencl/
chmod o+x $MOD_DEST/Python/3.6.6/pyopencl/

# End
echo 'DONE'
exit 0

