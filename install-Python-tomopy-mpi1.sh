#!/bin/bash

SW_DEST=/mxn/groups/pub/sw/pkg/GCC-5.4.0-2.26-OpenMPI-1.10.3
MOD_DEST=/mxn/groups/pub/sw/modules/maxiv

PROJECT=Python-2.7.12-tomopy-1.0.2

# Check if swadmin does not have ~/.local directory
DIRECTORY=~/.local
if [ -d "$DIRECTORY" ]; then
  echo 'Error: You have directory '$DIRECTORY', there is a risk it contains python modules.'
	exit -1
fi

# Ensure destiantion is in PYTHONPATH and that it exists
export PYTHONPATH=$SW_DEST/$PROJECT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p $SW_DEST/$PROJECT/lib/python2.7/site-packages

# Start installing
module use /mxn/groups/pub/sw/modules/maxiv
module load GCC/5.4.0-2.26 OpenMPI/1.10.3 Python/2.7.12/dxchange/0.1.3

echo 'Installing project: '$PROJECT' with '$(python -V 2>&1)

# --- tomopy ----
python -c "import tomopy"
if [ $? -eq 0 ]
then
	echo "tomopy python module already available, skipping installation tomopy"
else
	rm -rfd tomopy
	git clone https://github.com/tomopy/tomopy.git
	CWD=$(pwd)
	cd tomopy
	python setup.py install --prefix=$SW_DEST/$PROJECT
	cd $CWD
fi

# Create module
mkdir -p $MOD_DEST/Python/2.7.12/tomopy
cp modules/Python-2.7.12-tomopy-1.0.2.lua $MOD_DEST/Python/2.7.12/tomopy/1.0.2.lua
sed -i -e 's/\/mxn\/groups\/pub\/sw/\/mxn\/groups\/pub\/sw\/pkg\/GCC-5.4.0-2.26-OpenMPI-1.10.3/g' $MOD_DEST/Python/2.7.12/tomopy/1.0.2.lua

# End
echo 'DONE'
exit 0

